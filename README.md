## 说明

- 为了解决国内安装scoop时，下载慢的问题

- 本仓库不会经常更新

- 官方脚本

  ```sh
  Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
  
  # or shorter
  iwr -useb get.scoop.sh | iex
  ```

  

## 使用

- 打开powershell，获取权限

  ```powershell
  Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
  ```

- 修改安装目录

  ```powershell
  $env:SCOOP='D:\Applications\Scoop'
  [Environment]::SetEnvironmentVariable('SCOOP', $env:SCOOP, 'User')
  # run the installer
  ```

- 安装

  ```powershell
  iwr -useb https://gitee.com/fulsun/scoop-main/raw/master/install.ps1 | iex
  ```

  

## 软件安装

- 安装Git和7zip

  ```sh
  scoop install https://gitee.com/fulsun/scoop-main/raw/master/buckets/main/7zip.json
  
  scoop install https://gitee.com/fulsun/scoop-main/raw/master/buckets/main/git.json
  ```

  

